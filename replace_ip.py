import os
import re

with open('logs/ip.log', 'r') as file_ip:
    data = re.search(r'(\d{1,3}[\.]){3}\d{1,3}', file_ip.read())
    ip_new = data.group(0)

with open('ansible/hosts', 'r') as file_host:
    data = re.search(r'(\d{1,3}[\.]){3}\d{1,3}', file_host.read())
    ip_old = data.group(0)
    file_host.seek(0)

    with open('ansible/hosts_temp', 'w') as file_out:
        for i_line in file_host:
            if ip_old not in i_line:
                file_out.write(i_line)
            elif ip_old in i_line: 
                temp = i_line.replace(ip_old, ip_new)
                file_out.write(temp)

os.remove('./ansible/hosts')
os.rename('./ansible/hosts_temp', './ansible/hosts')
