#! /bin/bash
cd ./terraform && terraform apply -auto-approve && terraform output > ../logs/ip.log
sleep 5
cd .. && python3 replace_ip.py
sleep 5
cd ./ansible && ansible-playbook app.yaml -b
